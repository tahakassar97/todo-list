const jsonServer = require('json-server');

const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();

server.use(jsonServer.bodyParser);
server.use(middlewares);

server.get('/todos', (req, res) => {
  const { q, date, _page, _limit } = req.query;
  let todos = router.db.get('todos').value();

  if (q) {
    todos = todos.filter((todo) =>
      todo.title?.toLowerCase().includes(q?.toLowerCase())
    );
  }

  if (date) {
    const decodedDateString = decodeURIComponent(date);

    todos = todos.filter((todo) => {
      return (
        new Date(todo.date).toLocaleDateString('en-US', {
          timeZone: 'UTC',
        }) ===
        new Date(decodedDateString).toLocaleDateString('en-US', {
          timeZone: 'UTC',
        })
      );
    });
  }

  // Apply pagination
  const page = parseInt(_page, 10) || 1;
  const limit = parseInt(_limit, 10) || 10;
  const startIndex = (page - 1) * limit;
  const endIndex = startIndex + limit;
  const paginatedTodos = todos.slice(startIndex, endIndex);

  // Prepare response data
  const totalItems = todos.length;

  res.json({
    data: paginatedTodos,
    pagination: {
      totalItems,
    },
  });
});

server.put('/updateTodo', (req, res) => {
  const taskId = req.body.id;
  const updatedData = req.body;

  let tasks = router.db.get('todos').value();

  const taskIndex = tasks.findIndex((task) => task.id === taskId);

  if (taskIndex !== -1) {
    tasks[taskIndex] = { ...tasks[taskIndex], ...updatedData };
    res.status(200).json(tasks[taskIndex]);
  } else {
    res.status(404).json({ error: 'Task not found' });
  }
});

server.use(router);

const PORT = 3000;
server.listen(PORT, () => {
  console.log(`JSON Server is running on port ${PORT}`);
});
