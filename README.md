 
1. 🪔 I've created this app to showcase my coding skills. 

2. Simple Todo-List using React JS and Vite build tool.
3. Integrated with json-server to manage todos CRUD.

4. 🪔 To run the project clone it -> hit (yarn) -> then hit yarn dev

5. _Demo: https://todo-list.taha-kassar.atconcept.tech/_
