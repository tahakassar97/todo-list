export const DEFAULT_PAGE_SIZE = 20;

export const defaultNotificationOpts = {
  reverseOrder: false,
  style: {
    marginTop: '60px',
  },
  blank: {
    style: {
      background: '#3AB8FF',
      color: '#fff',
      padding: '10px 15px',
      borderRadius: '16px',
      fontSize: '12px',
      weight: 600,
    },
  },
  error: {
    style: {
      background: '#FF3D3D',
      color: '#fff',
      padding: '10px 15px',
      borderRadius: '16px',
      fontSize: '12px',
      weight: 600,
    },
    iconTheme: {
      primary: 'white',
      secondary: '#FF3D3D',
    },
  },
  success: {
    style: {
      background: '#2FB373',
      color: '#fff',
      padding: '10px 15px',
      borderRadius: '16px',
      fontSize: '12px',
      weight: 600,
    },
    iconTheme: {
      primary: 'white',
      secondary: '#2FB373',
    },
  },
};

export const dateFormat: Intl.DateTimeFormatOptions = {
  day: '2-digit',
  month: '2-digit',
  year: 'numeric',
  timeZone: 'UTC',
};

export function isEmptyObject(obj: object) {
  return Object.keys(obj).length === 0;
}

export function isEquals(obj1: unknown, obj2: unknown): boolean {
  if (obj1 === obj2) return true;

  if (
    typeof obj1 !== 'object' ||
    typeof obj2 !== 'object' ||
    obj1 === null ||
    obj2 === null
  )
    return false;

  const keys1 = Object.keys(obj1);
  const keys2 = Object.keys(obj2);

  if (keys1.length !== keys2.length) return false;

  for (const key of keys1) {
    if (
      !Object.prototype.hasOwnProperty.call(obj2, key) ||
      !isEquals(
        (obj1 as Record<string, unknown>)[key],
        (obj2 as Record<string, unknown>)[key]
      )
    )
      return false;
  }

  return true;
}
