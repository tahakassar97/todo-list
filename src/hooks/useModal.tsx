import { ElementType, FC, ReactNode, useCallback, useState } from 'react';

import classNames from 'classnames';

import { useLockPage } from './useLockPage';

import './use-modal.css';

interface ModalWrapperProps {
  className?: string;
  isLockScroll?: boolean;
  hideOverlay?: boolean;
  children: ReactNode;
  onConfirm?: () => void;
}

export interface ModalResult {
  open: () => void;
  close: () => void;
  toggle: () => void;
  isOpen: boolean;
}

const useModal = (
  opened = false,
  isLockScroll = true
): [ElementType<ModalWrapperProps>, ModalResult] => {
  const [isOpen, setOpen] = useState(opened || false);

  const { lockScroll, unlockScroll } = useLockPage();

  const open = useCallback(() => {
    if (isLockScroll) lockScroll();

    setOpen(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLockScroll]);

  const close = useCallback(() => {
    setOpen(false);

    if (isLockScroll) unlockScroll();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLockScroll]);

  const toggle = useCallback(() => {
    if (isLockScroll) {
      if (isOpen) unlockScroll();

      if (!isOpen) lockScroll();
    }

    setOpen((prevState) => !prevState);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLockScroll, isOpen]);

  const ModalWrapper: FC<ModalWrapperProps> = useCallback(
    ({ children, className = '', hideOverlay }) => {
      return (
        <>
          <div
            className={classNames(`fixed z-40 overflow-y-auto`, {
              'inset-0 scale-100': isOpen,
              'scale-0': !isOpen,
            })}
          >
            {isOpen && !hideOverlay && (
              <div
                className="fixed inset-0 h-full w-full bg-black z-20 opacity-40"
                onClick={close}
              ></div>
            )}
            <div className="flex items-center min-h-screen px-4 py-8">
              <div
                className={classNames(
                  `w-full max-w-4xl p-5 z-30 mx-auto transition-300 bg-white rounded-md shadow-lg ${className}`,
                  {
                    'open-modal': isOpen,
                    'close-modal': !isOpen,
                  }
                )}
              >
                {children}
              </div>
            </div>
          </div>
        </>
      );
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isOpen]
  );

  return [ModalWrapper, { open, close, toggle, isOpen }];
};

export { useModal };
