import { useCallback, useId } from 'react';
import toast, {
  DefaultToastOptions,
  Renderable,
  resolveValue,
  ValueOrFunction,
  ToastOptions,
  Toast,
} from 'react-hot-toast';

type ToastHandler = (
  message: ValueOrFunction<Renderable, Toast>,
  options?: ToastOptions
) => string;

interface Props {
  [key: string]: unknown;
}

const useNotify = (defaultKey?: string, override = true) => {
  const id = useId();

  if (!defaultKey) defaultKey = id;

  // eslint-disable-next-line @typescript-eslint/ban-types
  const baseNotification = useCallback(
    (
      action: ToastHandler,
      message: Renderable,
      props?: Props,
      opts?: DefaultToastOptions
    ) => {
      let currentMessage = message;

      if (currentMessage && typeof currentMessage === 'string') {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        currentMessage = message;
      }

      return action(currentMessage, {
        ...opts,
        id: override ? opts?.id || defaultKey : opts?.id,
        position: 'top-center',
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const success = (
    message: Renderable,
    props?: Props,
    opts?: DefaultToastOptions
  ) => baseNotification(toast.success, message, props, opts);

  const error = (
    message: Renderable,
    props?: Props,
    opts?: DefaultToastOptions
  ) => baseNotification(toast.error, message, props, opts);

  const info = (
    message: Renderable,
    props?: Props,
    opts?: DefaultToastOptions
  ) => baseNotification(toast, message, props, opts);

  const loading = (
    message: Renderable,
    props?: Props,
    opts?: DefaultToastOptions
  ) => baseNotification(toast.loading, message, props, opts);

  const dismiss = (toastId = defaultKey) => toast.dismiss(toastId);

  const dismissAll = () => toast.dismiss();

  const remove = (toastId = defaultKey) => toast.remove(toastId);

  const removeAll = () => toast.remove();

  const promise = <T,>(
    promise: Promise<T>,
    msgs: {
      loading: NonNullable<Renderable>;
      success: ValueOrFunction<Renderable, T>;
      error: ValueOrFunction<Renderable, unknown>;
    },
    props?: Props,
    opts?: DefaultToastOptions
  ) => {
    const id = loading(msgs.loading, props, { ...opts, ...opts?.loading });

    promise
      .then((p) => {
        success(resolveValue(msgs.success, p), props, {
          id,
          ...opts,
          ...p,
        });
        return p;
      })
      .catch((e) => {
        error(resolveValue(msgs.error, e), props, {
          id,
          ...opts,
          ...e,
        });
      });

    return promise;
  };

  return {
    base: toast,
    success,
    error,
    info,
    loading,
    promise,
    dismiss,
    dismissAll,
    remove,
    removeAll,
  };
};

export { useNotify };
