import { useSearchParams } from 'react-router-dom';

export const useCustomSearchParams = () => {
  const [search, setSearchParams] = useSearchParams();

  const searchAsObject = Object.fromEntries(new URLSearchParams(search));

  const setSearch = ({ ...param }, replace = false) => {
    const newSearch = { ...searchAsObject, ...param };

    setSearchParams(newSearch, { replace });
  };

  const deleteParam = (key: string) => {
    search.delete(key);

    setSearchParams(search, {
      replace: true,
    });
  };

  const reset = () =>
    setSearchParams('', {
      replace: true,
    });

  return {
    search: searchAsObject,
    setSearch,
    deleteParam,
    reset,
  };
};
