import { ChangeEvent, FC } from 'react';

interface Props {
  title: string;
  value: string;
  name?: string;
  disabled?: boolean;
  checked?: boolean;
  className?: string;
  defaultChecked?: boolean;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

const Checkbox: FC<Props> = ({
  name,
  title,
  value,
  disabled,
  checked,
  className = '',
  defaultChecked,
  onChange,
}) => {
  return (
    <div className={`checkbox flex items-center ${className}`}>
      <label className="inline-flex items-center w-fit cursor-pointer">
        <input
          type="checkbox"
          className="checkbox__input transition_300 cursor-pointer"
          value={value}
          name={name}
          checked={checked}
          disabled={disabled}
          defaultChecked={defaultChecked}
          onChange={onChange}
        />
        <span className="checkbox__in cursor-pointer gap-x-2 text-sm font-semibold">
          <span className="checkbox__tick"></span>
          {title ? <span className="checkbox__text">{title}</span> : null}
        </span>
      </label>
    </div>
  );
};

export { Checkbox };
