export interface InputProps
  extends Omit<React.InputHTMLAttributes<HTMLInputElement>, 'name'> {
  name: string;
  label?: string;
  inputClassName?: string;
  errorName?: string;
}
