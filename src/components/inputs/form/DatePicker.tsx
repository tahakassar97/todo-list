import { FC, ReactNode, useEffect } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import DatePicker from 'react-datepicker';

import { InputProps } from '../IProps';
import { Label } from '../../labels';
import { Icon } from '../../icons';
import ErrorMessage from './ErrorMessage';

import 'react-datepicker/dist/react-datepicker.css';
import './styles.css';

interface DatePickerInputProps
  extends Omit<InputProps, 'type' | 'value' | 'defaultValue'> {
  showTimeSelect?: boolean;
  dateFormat?: string;
  minDate?: Date | string;
  minTime?: Date | string;
  maxDate?: Date;
  maxTime?: Date;
  defaultValue?: Date;
  renderCustomHeader?: ReactNode;
  showYearDropdown?: boolean;
}

const DateInput: FC<DatePickerInputProps> = ({
  name = '',
  label,
  required = true,
  disabled = false,
  className = '',
  placeholder,
  dateFormat,
  minDate, // either static date or input name date
  maxDate,
  showYearDropdown = true,
  defaultValue,
}) => {
  const {
    control,
    setValue,
    formState: { errors },
  } = useFormContext();

  const inputError = errors[name];

  useEffect(() => {
    if (defaultValue) setValue(name, defaultValue);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [defaultValue]);

  let _minDate: Date;

  if (minDate instanceof Date) _minDate = minDate;

  return (
    <div className={`flex w-full flex-col ${className}`}>
      {label && (
        <span className="flex gap-x-1 mb-1">
          <Label label={label} />
          {required && <Label label="*" className="text-red-500" />}
        </span>
      )}

      <div className="relative">
        <Icon
          name="chevron-down"
          className="w-5 h-5 absolute top-4 mt-0.5 z-10 right-2"
        />

        <Controller
          control={control}
          name={name}
          render={({ field: { ref, onChange, value } }) => {
            const handleChange = (date: Date) => {
              onChange(date);
            };

            return (
              <DatePicker
                placeholderText={placeholder || 'Select date'}
                selected={
                  value || (defaultValue ? new Date(defaultValue) : null)
                }
                disabled={disabled}
                required={required}
                dateFormat={dateFormat ?? 'MMMM d, yyyy'}
                minDate={_minDate}
                showYearDropdown={showYearDropdown}
                yearDropdownItemNumber={15}
                scrollableYearDropdown
                maxDate={maxDate}
                showTimeSelect={false}
                className="primary-input custom-datepicker"
                onChange={handleChange}
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                ref={(_ref: any) => {
                  ref({
                    focus: _ref?.setFocus,
                  });
                }}
              />
            );
          }}
        />
      </div>

      <ErrorMessage inputError={inputError} />
    </div>
  );
};

export default DateInput;
