import { FC } from 'react';
import { Controller, useFormContext } from 'react-hook-form';

import { Label } from '@components/labels';

import { InputProps } from '../IProps';

interface CheckboxProps extends Omit<InputProps, 'type' | 'name'> {
  name: string;
  label: string;
}

const CheckboxInput: FC<CheckboxProps> = ({
  name,
  value,
  label,
  className = '',
  required,
  onChange: _onChange,
  ...rest
}) => {
  const { control } = useFormContext();

  return (
    <div className={`checkbox w-fit ${className}`}>
      <label className="flex items-center">
        <Controller
          control={control}
          name={name}
          rules={{ required: required }}
          render={({ field: { onChange, ..._rest } }) => {
            return (
              <input
                type="checkbox"
                id={`id-${value}`}
                className="checkbox__input transition_300 cursor-pointer"
                onChange={(e) => {
                  if (_onChange) {
                    _onChange(e);
                    return;
                  }
                  onChange(e);
                }}
                required={required}
                {...rest}
                {..._rest}
              />
            );
          }}
        />
        <span className="checkbox__in cursor-pointer text-sm font-semibold">
          <span className="checkbox__tick"></span>
          <Label
            className="checkbox__text cursor-pointer rtl:mr-1"
            label={label}
            name={`id-${value}`}
          />
        </span>
      </label>
    </div>
  );
};

export default CheckboxInput;
