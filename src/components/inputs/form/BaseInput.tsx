import { FC } from 'react';
import { useFormContext } from 'react-hook-form';

import classNames from 'classnames';

import { InputProps } from '../IProps';
import { Label } from '../../index';
import ErrorMessage from './ErrorMessage';

const BaseInput: FC<InputProps> = ({
  label,
  className = '',
  name,
  type,
  inputClassName = '',
  required = true,
  ...rest
}) => {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  const inputError = errors[name];

  return (
    <div className={`flex flex-col relative ${className}`}>
      {label ? (
        <span className="flex gap-x-1">
          <Label label={label} name={name} />
          {required ? <Label label="*" className="text-red-500" /> : null}
        </span>
      ) : null}

      <input
        className={classNames(`primary-input mt-1 ${inputClassName}`, {
          'ring-red-600': inputError,
        })}
        autoComplete="off"
        id={name}
        {...register(`${name}`, {
          ...(type === 'number' && { valueAsNumber: true }),
          required,
          max: rest.max,
        })}
        type={type}
        required={required}
        {...rest}
      />

      <ErrorMessage inputError={inputError} />
    </div>
  );
};

export default BaseInput;
