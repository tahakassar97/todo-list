import { FC } from 'react';
import { FieldError, Merge, FieldErrorsImpl } from 'react-hook-form';

interface Props {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  inputError?: FieldError | Merge<FieldError, FieldErrorsImpl<any>>;
}

const ErrorMessage: FC<Props> = ({ inputError }) => {
  return (
    <>
      {inputError ? (
        <p>
          {`* ${inputError?.message
            ?.toString()
            .slice(0, 1)
            .toUpperCase()}${inputError?.message?.toString().slice(1)}`}
        </p>
      ) : null}
    </>
  );
};

export default ErrorMessage;
