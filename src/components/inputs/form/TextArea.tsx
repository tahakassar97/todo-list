import { FC, TextareaHTMLAttributes } from 'react';
import { useFormContext } from 'react-hook-form';

import classNames from 'classnames';

import { Label } from '../../labels';
import ErrorMessage from './ErrorMessage';

interface TextAreaProps
  extends Omit<TextareaHTMLAttributes<HTMLTextAreaElement>, 'name'> {
  name: string;
  className?: string;
  label?: string;
}

const TextArea: FC<TextAreaProps> = ({
  className,
  label,
  name,
  required = true,
  ...rest
}) => {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  const inputError = errors[name];

  return (
    <div className={`flex w-full flex-col ${className}`}>
      <div className="relative flex flex-col">
        {label && (
          <span className="flex gap-x-1">
            <Label label={label} />
            {required && <Label label="*" className="text-red-500" />}
          </span>
        )}
        <textarea
          className={classNames(
            `primary-input resize-none mt-1 ltr:pl-4 ltr:pr-2 rtl:pr-4 rtl:pl-2`,
            {
              'ring-red-600': inputError,
            }
          )}
          required={required}
          {...register(`${name}`)}
          {...rest}
        ></textarea>

        <ErrorMessage inputError={inputError} />
      </div>
    </div>
  );
};

export default TextArea;
