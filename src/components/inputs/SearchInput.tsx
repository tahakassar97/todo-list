import classNames from 'classnames';

import { Icon } from '@components';

import { InputProps } from './IProps';

const SearchInput: React.FC<InputProps> = ({
  name,
  inputClassName,
  className,
  ...rest
}) => {
  return (
    <div className={`relative ${className}`}>
      <Icon
        name="search"
        className={`absolute top-3 text-gray-400 left-3 cursor-pointer`}
      />

      <input
        className={classNames(
          `rounded-md outline-none py-2 text-sm bg-transparent border focus:border-gray-400 transition-300 border-gray-300 mt-1 pl-10 w-[216px] ${inputClassName}`
        )}
        id={name}
        onKeyDown={(e) => {
          e.key === 'Enter' && e.preventDefault();
        }}
        {...rest}
      />
    </div>
  );
};

export { SearchInput };
