import { ButtonHTMLAttributes, FC, MutableRefObject } from 'react';

interface Props extends Omit<ButtonHTMLAttributes<HTMLButtonElement>, 'ref'> {
  title: string;
  _ref?: MutableRefObject<HTMLButtonElement>;
}

const Button: FC<Props> = ({
  title,
  type = 'button',
  className,
  disabled = false,
  children,
  _ref,
  onClick,
}) => {
  return (
    <button
      className={`${className} ${
        disabled &&
        '!bg-gray-300 !text-white cursor-not-allowed hover:!bg-gray-300 hover:!text-white hover:!shadow-none !border-none'
      }`}
      type={type}
      disabled={disabled}
      ref={_ref}
      onClick={onClick}
    >
      {title}

      {children}
    </button>
  );
};

export default Button;
