import { FC } from 'react';

const Pause: FC = () => {
  return (
    <>
      <path d="M10 1C14.9706 1 19 5.02944 19 10C19 14.9706 14.9706 19 10 19C5.02944 19 1 14.9706 1 10C1 5.02944 5.02944 1 10 1Z" />
      <path d="M13 13H7V7H13V13Z" />
    </>
  );
};

export default Pause;
