import React, { FC } from 'react';

const Frequency: FC = () => {
  return (
    <>
      <path
        d="M4 7.5V14.5"
        stroke="#FBBB21"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M7.5 5.75V16.25"
        stroke="#FBBB21"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M11 3.125V18.875"
        stroke="#FBBB21"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M14.5 5.75V16.25"
        stroke="#FBBB21"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M18 7.5V14.5"
        stroke="#FBBB21"
        strokeWidth="2"
        strokeLinecap="round"
      />
    </>
  );
};

export default Frequency;
