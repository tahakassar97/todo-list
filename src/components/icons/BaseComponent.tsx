import { FC } from 'react';

import { Props } from './IProps';

import Plus from './Plus2';
import Search from './Search';
import List from './List';
import Calender from './Calender';
import Settings from './Settings';
import Logout from './Logout';
import Dashboard from './Dashboard';
import ChevronDown from './ChevronDown';
import Trash from './Trash';
import Close from './Close';

const Icon: FC<Props> = ({
  className,
  styles,
  height,
  width,
  name,
  viewBox,
  onClick,
}) => {
  const getIcon = () => {
    switch (name) {
      case 'plus': {
        return <Plus />;
      }

      case 'search': {
        return <Search />;
      }

      case 'list': {
        return <List />;
      }

      case 'calender': {
        return <Calender />;
      }

      case 'settings': {
        return <Settings />;
      }

      case 'logout': {
        return <Logout />;
      }

      case 'dashboard': {
        return <Dashboard />;
      }

      case 'chevron-down': {
        return <ChevronDown />;
      }

      case 'trash': {
        return <Trash />;
      }

      case 'close': {
        return <Close />;
      }
    }
  };

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
      width={width || '25'}
      height={height || '25'}
      viewBox={`0 0 ${viewBox ? viewBox[0] : width || '25'} ${
        viewBox ? viewBox[1] : height || '25'
      }`}
      className={className}
      style={styles}
      onClick={onClick}
    >
      {getIcon()}
    </svg>
  );
};

export default Icon;
