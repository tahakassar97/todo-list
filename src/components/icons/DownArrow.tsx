import { FC } from 'react';

const DownArrow: FC = () => {
  return <path d="M5 5L0 0H10L5 5Z" />;
};

export default DownArrow;
