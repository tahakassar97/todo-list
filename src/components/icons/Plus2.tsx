import { FC } from 'react';

const Plus2: FC = () => {
  return (
    <path d="M12.7969 5.7041V8.89062H0.126953V5.7041H12.7969ZM8.20117 0.689453V14.1465H4.73535V0.689453H8.20117Z" />
  );
};

export default Plus2;
