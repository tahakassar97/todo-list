import React, { ReactElement, useEffect } from 'react';

import { useAppMutation, useFetch } from '@api';
import { DEFAULT_PAGE_SIZE } from '@constants';
import { useNotify } from '@hooks';

interface Props {
  resource: string;
  children: React.ReactNode;
  onSuccess?: (data?: { [key: string]: unknown }) => void;
  transformBody?: (data: {
    [key: string]: unknown;
  }) => { [key: string]: unknown } | undefined;
}

const Create: React.FC<Props> = ({
  children,
  resource,
  onSuccess: _onSuccess,
  transformBody,
}) => {
  const { isLoading, isSuccess, isError, mutateAsync } = useAppMutation(
    resource,
    'POST'
  );

  const { success, error } = useNotify();

  useFetch(
    resource,
    {},
    {
      enabled: isSuccess,
      queryKey: [resource, { _limit: DEFAULT_PAGE_SIZE, _page: 1 }],
    }
  );

  const createOne = React.useCallback(
    (_data: { [key: string]: unknown }) => {
      const data = transformBody ? transformBody?.(_data) : { ..._data };

      mutateAsync({
        ...data,
      });
    },

    [mutateAsync, transformBody]
  );

  useEffect(() => {
    if (isSuccess) {
      if (_onSuccess) _onSuccess();

      if (!_onSuccess) {
        success('Created successfully!');
      }
    }

    if (isError) {
      error('Something went wrong!');
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isError, isSuccess]);

  return (
    <>
      {React.isValidElement(children) &&
        React.Children.map(children, (child) => {
          return React.cloneElement(
            child as ReactElement<{
              isLoading: boolean;
              onSubmit: (_data: { [key: string]: unknown }) => unknown;
            }>,
            {
              isLoading: isLoading,
              onSubmit: createOne,
            }
          );
        })}
    </>
  );
};

export default Create;
