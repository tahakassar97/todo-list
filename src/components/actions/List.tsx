/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import { useFetch } from '@api';
import { useCustomSearchParams, useNotify } from '@hooks';
import { DEFAULT_PAGE_SIZE, isEmptyObject } from '@constants';
import { Pagination } from '@components/grid';

interface Props {
  resource: string;
  children: React.ReactNode;
}

interface ChildProps {
  data: { [key: string]: any }[];
  pageSize: number;
  totalCount: number;
  onDelete: VoidFunction;
}

interface IResponse {
  pagination: {
    totalItems: number;
  };

  data: { [key: string]: any }[];
}

const List: React.FC<Props> = ({ resource, children }) => {
  const { search } = useCustomSearchParams();
  const { id } = useParams();

  const { data, isLoading, isError, refetch } = useFetch<IResponse>(
    resource,
    {
      ...(!isEmptyObject(search)
        ? {
            ...search,
            _limit: DEFAULT_PAGE_SIZE,
          }
        : {
            _page: 1,
            _limit: DEFAULT_PAGE_SIZE,
          }),
    },
    {
      queryKey: [
        resource,
        { ...search, _limit: DEFAULT_PAGE_SIZE, _page: search.page || 1, id },
      ],
    }
  );

  const { error } = useNotify();

  useEffect(() => {
    if (isError) {
      error('Something went wrong!');
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isError]);

  const onDelete = () => refetch();

  if (isLoading) {
    return (
      <div className="h-screen w-screen fixed inset-0 flex justify-center items-center">
        Loading ...
      </div>
    );
  }

  return (
    <>
      {data && !isError
        ? React.Children.map(children, (child) => {
            if (React.isValidElement(child)) {
              return (
                <>
                  {React.cloneElement(
                    child as unknown as React.ReactElement<ChildProps>,
                    {
                      data: data.data,
                      onDelete,
                    }
                  )}

                  <Pagination
                    pageSize={DEFAULT_PAGE_SIZE}
                    totalCount={data.pagination.totalItems}
                  />
                </>
              );
            }
          })
        : null}
    </>
  );
};

export default List;
