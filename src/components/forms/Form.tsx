import {
  FC,
  ReactNode,
  useEffect,
  isValidElement,
  cloneElement,
  ReactElement,
} from 'react';
import { FieldValues, SubmitHandler, useFormContext } from 'react-hook-form';

import { isEquals } from '@constants';

import { Icon } from '../icons';
import { Button } from '../buttons';
import { withFormContext } from './hoc';

interface Props {
  className?: string;
  title?: string;
  children: ReactNode;
  Button?: ReactNode | null;
  record?: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  };
  isLoading?: boolean;
  onSubmit?: SubmitHandler<FieldValues>;
}

const Form: FC<Props> = ({
  children,
  Button: CustomButton,
  title = '',
  className,
  record,
  isLoading = false,
  onSubmit = () => {
    //
  },
}) => {
  const { handleSubmit, reset, watch } = useFormContext();

  const watchFields = watch();

  useEffect(() => {
    if (record && !isEquals(watchFields, record)) reset(record);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [record]);

  const buttonProps = {
    disabled: isLoading,
    children: isLoading && (
      <Icon name="loader" viewBox={[90, 90]} width="21px" height="21px" />
    ),
  };

  return (
    <form className={`${className}`} onSubmit={handleSubmit(onSubmit)}>
      {children}

      {CustomButton !== undefined ? (
        isValidElement(CustomButton) &&
        cloneElement(
          CustomButton as ReactElement<typeof buttonProps>,
          buttonProps
        )
      ) : (
        <Button
          className="mt-4 secondary-btn gap-x-1"
          disabled={isLoading}
          title={title || 'Submit'}
          type="submit"
        >
          {isLoading && <>Loading ...</>}
        </Button>
      )}
    </form>
  );
};

const _Form = withFormContext(Form);

export { _Form as Form };
