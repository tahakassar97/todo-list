import { ComponentType, FC } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

import { AnyObjectSchema } from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

interface IHookFormProps {
  schema?: AnyObjectSchema;
}

export const withFormContext =
  <P extends object>(Form: ComponentType<P>): FC<P & IHookFormProps> =>
  ({ ...props }: IHookFormProps) => {
    const schema = props.schema;

    const methods = useForm({
      resolver: schema ? yupResolver(props.schema) : undefined,
    });

    return (
      <FormProvider {...methods}>
        <Form {...(props as P)} />
      </FormProvider>
    );
  };
