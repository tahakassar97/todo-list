/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

import { Checkbox } from '../inputs';
import { Button } from '../buttons';
import { Label } from '../labels';
import { Icon } from '../icons';
import { useCardGrid } from './useCardGrid';

export interface IPropsActions {
  actions?: {
    onToggle: (id: number, checked: boolean) => void;
    onCreate?: VoidFunction;
  };
  onDelete?: VoidFunction;
}

interface Props extends IPropsActions {
  data?: {
    [key: string]: any;
  }[];
  colors?: string[];
  resource: string;
}

const CardGrid: React.FC<Props> = ({
  data = [],
  actions,
  resource,
  onDelete: refetchData,
}) => {
  const {
    DeleteModal,
    toggle,
    getDate,
    onCreate,
    onSelectItem,
    onDelete,
  } = useCardGrid({
    actions,
    resource,
    refetchData,
  });

  return (
    <div>
      <DeleteModal className="!max-w-md">
        <>
          <Label label="Are you sure to delete the task?" />

          <div className="flex justify-end w-full mt-8 gap-x-3">
            <Button title="Cancel" className="outlined-btn" onClick={toggle} />
            <Button
              title="Delete"
              className="btn bg-red-500 border-red-500 hover:shadow-lg"
              onClick={onDelete}
            />
          </div>
        </>
      </DeleteModal>

      <ul className="flex flex-wrap gap-2 md:gap-5 p-2 md:p-5 border rounded-md md:mx-3">
        {React.Children.toArray(
          data.map((item) => (
            <li
              className="card bg-highlight/20"
            >
              <p className="flex justify-between font-bold text-lg">
                <strong>{getDate(item.date)}</strong>

                <Icon
                  name="trash"
                  width="16"
                  height="16"
                  className="text-red-600 mt-0.5 cursor-pointer"
                  onClick={() => {
                    toggle();
                    onSelectItem(item.id);
                  }}
                />
              </p>
              <div className="flex mt-4 items-start space-x-3">
                <Checkbox
                  title=""
                  value={item.id}
                  defaultChecked={item.completed}
                  onChange={(e) => actions?.onToggle(item.id, e.target.checked)}
                />

                <p>{item.title}</p>
              </div>
            </li>
          ))
        )}

        <li
          className="card justify-center items-center bg-gray-100 cursor-pointer group transition-300"
          onClick={onCreate}
        >
          <Icon
            name="plus"
            className="text-highlight transition group-hover:scale-110"
            width="50"
            height="50"
            viewBox={[20, 20]}
          />
        </li>
      </ul>
    </div>
  );
};

export default CardGrid;
