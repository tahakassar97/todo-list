import React from 'react';
import { useNavigate } from 'react-router-dom';

import { dateFormat } from '@constants';
import { useDelete } from '@api';
import { useModal, useNotify } from '@hooks';

import { IPropsActions } from './CardGrid';

interface Props extends IPropsActions {
  resource: string;
  refetchData: VoidFunction | undefined;
}

const useCardGrid = ({ actions, resource, refetchData }: Props) => {
  const navigate = useNavigate();

  const [selectedItem, setSelectedItem] = React.useState(-1);

  const { mutateAsync } = useDelete(`${resource}/${selectedItem}`);

  const [DeleteModal, { toggle }] = useModal();
  const { success } = useNotify();

  const getDate = React.useCallback((date: string) => {
    const _date = new Date(date);

    if (_date) return _date.toLocaleString('en-US', dateFormat);

    return new Date().toLocaleString('en-US', dateFormat);
  }, []);

  const onCreate = React.useCallback(() => {
    if (actions?.onCreate) actions.onCreate();

    if (!actions?.onCreate) navigate('create');

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [actions]);

  const onSelectItem = React.useCallback((id: number) => {
    setSelectedItem(id);
  }, []);

  const onDelete = React.useCallback(async () => {
    await mutateAsync();

    success('Deleted successfully!');

    toggle();

    refetchData?.();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mutateAsync, refetchData, toggle]);

  return {
    selectedItem,
    DeleteModal,
    getDate,
    onCreate,
    onSelectItem,
    onDelete,
    toggle,
  };
};

export { useCardGrid };
