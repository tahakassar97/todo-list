import { useCustomSearchParams } from '@hooks';

const usePagination = () => {
  const { search, setSearch } = useCustomSearchParams();

  const onChange = (page: number) => {
    setSearch({ page }, true);
  };

  return {
    search,
    onChange,
  };
};

export { usePagination };
