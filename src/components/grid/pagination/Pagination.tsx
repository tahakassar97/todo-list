import { FC } from 'react';
import Pagination from 'rc-pagination';

import { usePagination } from './usePagination';

import 'rc-pagination/assets/index.css';

interface IPaginationProps {
  totalCount: number;
  pageSize: number;
}

const MainPagination: FC<IPaginationProps> = ({ totalCount, pageSize }) => {
  const { onChange, search } = usePagination();

  return (
    <section className="flex justify-center py-5 font-semibold w-55">
      <Pagination
        showTitle={false}
        current={+search.page || 1}
        total={totalCount}
        pageSize={pageSize}
        showLessItems
        onChange={onChange}
      />
    </section>
  );
};

export default MainPagination;
