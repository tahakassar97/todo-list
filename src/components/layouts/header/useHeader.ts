import { useCustomSearchParams } from '@hooks';

const useHeader = () => {
  const { search } = useCustomSearchParams();

  const title = search['date'] ? 'Today Tasks' : 'All Tasks';

  return {
    title,
  };
};

export default useHeader;
