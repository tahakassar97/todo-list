import React from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import useHeader from './useHeader';
import { Icon } from '../../icons';

const Header: React.FC = () => {
  const navigate = useNavigate();
  const { hash, search } = useLocation();

  const { title } = useHeader();

  return (
    <header className="flex justify-between items-center text-4xl font-bold w-full p-6">
      <h5>{title}</h5>

      <Icon
        name="list"
        className="md:hidden"
        onClick={() =>
          navigate({
            hash: hash ? '' : 'open-sidebar',
            search,
          })
        }
      />
    </header>
  );
};

export default Header;
