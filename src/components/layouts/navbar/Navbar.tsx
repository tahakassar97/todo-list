import React from 'react';

import classNames from 'classnames';

import { SearchInput } from '../../inputs';
import { Icon } from '@components';
import { TIconName } from '@components/icons/IProps';
import { useNavbar } from './useNavbar';

interface Props {
  className?: string;
}

const Navbar: React.FC<Props> = ({ className }) => {
  const { search, items, isOpen, onChange, onClickItem, closeNavbar } =
    useNavbar();

  return (
    <>
      <aside
        className={classNames(
          `fixed top-0 w-64 flex flex-col justify-between h-screen rounded-tr-[2.5rem] rounded-br-[2.5rem] drop-shadow-md transition-300 bg-gray-200 p-5 text-gray-500 z-20 ${className}`,
          {
            '-left-full md:left-0': !isOpen,
            'left-0': isOpen,
          }
        )}
      >
        <div>
          <span className="flex justify-between items-center">
            <h6 className="text-xl font-bold">Menu</h6>

            <Icon name="close" className="text-black md:hidden" onClick={closeNavbar} />
          </span>
          <SearchInput
            name="search"
            placeholder="Search"
            className="mt-4 mb-10"
            defaultValue={[search['q']]}
            onChange={onChange}
          />

          <small className="uppercase text-[10px] font-bold">Tasks</small>

          <ul className="flex flex-col space-y-2 mt-3">
            {React.Children.toArray(
              items.map(({ icon, title, isActive, date, className }) => (
                <li
                  className={classNames(
                    'flex w-full items-center gap-x-3 p-2 cursor-pointer transition-300 rounded-md',
                    {
                      'hover:bg-gray-400 hover:text-white': !isActive,
                      'bg-gray-400 text-white': isActive,
                    }
                  )}
                  onClick={() => onClickItem(date)}
                >
                  <Icon
                    name={icon as TIconName}
                    className={className}
                    width="14"
                    height="14"
                    viewBox={[20, 20]}
                  />
                  {title}
                </li>
              ))
            )}
          </ul>
        </div>

        <div>
          <ul className="flex flex-col space-y-4 mt-3 px-2">
            <li className="flex w-full items-center gap-x-3 cursor-not-allowed">
              <Icon
                name="settings"
                className="text-gray-500 mt-0.5"
                width="15"
                height="15"
                viewBox={[20, 20]}
              />
              Settings
            </li>

            <li className="flex w-full items-center gap-x-3 cursor-not-allowed">
              <Icon
                name="logout"
                className="text-gray-500"
                width="15"
                height="15"
              />
              Logout
            </li>
          </ul>
        </div>
      </aside>

      {isOpen ? (
        <div
          className="inset-0 fixed w-screen h-screen z-10 bg-black/25"
          onClick={closeNavbar}
        ></div>
      ) : null}
    </>
  );
};

export default Navbar;
