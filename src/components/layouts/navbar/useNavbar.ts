import React from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { useCustomSearchParams } from '@hooks';

const useNavbar = () => {
  const { search, setSearch, deleteParam } = useCustomSearchParams();
  const { hash, search: _search } = useLocation();
  const navigate = useNavigate();

  const items = React.useMemo(() => {
    return [
      {
        title: 'All',
        icon: 'dashboard',
        date: null,
        isActive: !search['date'],
        className: 'mt-1',
      },
      {
        title: 'Today',
        icon: 'calender',
        date: new Date().toLocaleDateString(),
        isActive:
          search['date'] && search['date'] === new Date().toLocaleDateString(),
      },
    ];
  }, [search]);

  const onChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setSearch({
        q: e.target.value,
      });
    },

    [setSearch]
  );

  const onClickItem = React.useCallback(
    (date: string | null) => {
      if (date)
        setSearch({
          date,
        });

      if (!date) deleteParam('date');
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [setSearch]
  );

  const toggleNavbar = React.useCallback(() => {
    navigate({
      hash: hash ? '' : 'open-navbar',
      search: _search,
    });
  }, [hash, navigate, _search]);

  const closeNavbar = React.useCallback(() => {
    navigate({
      hash: '',
      search: _search,
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_search]);

  const openNavbar = React.useCallback(() => {
    navigate({
      hash,
      search: _search,
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hash, _search]);

  return {
    search,
    items,
    isOpen: hash ? true : false,
    onChange,
    onClickItem,
    toggleNavbar,
    closeNavbar,
    openNavbar,
  };
};

export { useNavbar };
