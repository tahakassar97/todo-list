import { FC } from 'react';

import classNames from 'classnames';

export interface LabelProps {
  label: string;
  name?: string;
  className?: string;
}

const Label: FC<LabelProps> = ({ label, name, className }) => {
  return (
    <label
      htmlFor={name}
      className={classNames(`font-semibold text-sm ${className}`)}
    >
      {label}
    </label>
  );
};

export { Label };
