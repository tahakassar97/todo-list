import { useMutation } from '@tanstack/react-query';
import axios, { AxiosError, AxiosRequestConfig } from 'axios';

const baseUrl = import.meta.env.VITE_API_KEY;

export const useAppMutation = <TData, TError>(
  url: string,
  method: AxiosRequestConfig['method'] = 'POST'
) => {
  const mutation = useMutation<TData, AxiosError<TError>, TData>((data) =>
    axios({
      method,
      url: `${baseUrl}/${url}`,
      data,
    })
  );

  return mutation;
};
