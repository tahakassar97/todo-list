import { useMutation } from '@tanstack/react-query';
import axios from 'axios';

const baseUrl = import.meta.env.VITE_API_KEY;

export const useDelete = (url: string) => {
  const mutation = useMutation(() => axios.delete(`${baseUrl}/${url}`));

  return mutation;
};
