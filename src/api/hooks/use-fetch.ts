import {
  QueryFunctionContext,
  QueryKey,
  UseQueryOptions,
  useQuery,
} from '@tanstack/react-query';
import axios from 'axios';

export const baseUrl = import.meta.env.VITE_API_KEY;

export const useFetch = <T>(
  url: string,
  params?: object,
  config?: UseQueryOptions<T, Error, T, QueryKey>
) => {
  const context = useQuery<T, Error, T, QueryKey>(
    [url, params],
    ({ queryKey, meta }) => fetcher({ queryKey, meta }),
    {
      enabled: !!url,
      ...config,
    }
  );

  return context;
};

export const fetcher = async <T>({
  queryKey,
}: QueryFunctionContext): Promise<T> => {
  const [url, params] = queryKey;

  return axios
    .get<T>(`${baseUrl}/${url}`, { params: { ...(params as object) } })
    .then((res) => res.data);
};
