import { CardGrid, Header, List } from '@components';

import useCreateModal from './useCreateModal';
import useUpdateStatus from './useUpdateStatus';

function Main() {
  const [CreateModalComponent, { toggle }] = useCreateModal();
  const { onUpdate } = useUpdateStatus();

  return (
    <>
      <CreateModalComponent />

      <div className="md:ml-72">
        <Header />

        <List resource="todos">
          <CardGrid
            actions={{
              onCreate: toggle,
              onToggle: onUpdate,
            }}
            resource="todos"
          />
        </List>
      </div>
    </>
  );
}

export default Main;
