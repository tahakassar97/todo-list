import { useCallback } from 'react';

import { useAppMutation } from '@api';

const useUpdateStatus = () => {
  const { mutateAsync } = useAppMutation('updateTodo', 'PUT');

  const onUpdate = useCallback(
    (id: number, checked: boolean) => {
      mutateAsync({
        id,
        completed: checked,
      });
    },
    [mutateAsync]
  );

  return {
    onUpdate,
  };
};

export default useUpdateStatus;
