import { ElementType, FC, useCallback } from 'react';

import { ModalResult, useModal } from '@hooks';
import {
  Button,
  Create,
  DatePicker,
  Form,
  FormCheckbox,
  Label,
  TextArea,
} from '@components';

interface TodoBody {
  date: Date;
  completed: boolean;
  description: string;
}

const useCreateModal = (): [ElementType, ModalResult] => {
  const [CreateModal, { toggle, ...restProps }] = useModal();

  const transformBody = (data: TodoBody) => {
    return {
      date: data.date,
      completed: data.completed,
      title: data.description,
    };
  };

  const CreateModalComponent: FC = useCallback(() => {
    return (
      <CreateModal className="!max-w-md">
        <Create
          resource="todos"
          onSuccess={() => toggle()}
          transformBody={(data) => transformBody(data as unknown as TodoBody)}
        >
          <Form
            className="relative"
            Button={
              <Button
                title="Create"
                type="submit"
                className="absolute bottom-0 right-0 primary-btn"
              />
            }
          >
            <Label label="Create New Task" className="text-xl" />

            <div className="flex flex-col gap-y-5 my-6">
              <TextArea
                name="description"
                label="Task Description"
                placeholder="Task Description"
                required
                rows={5}
              />

              <DatePicker name="date" />

              <FormCheckbox
                label="Completed Task"
                name="completed"
                required={false}
              />
            </div>

            <div className="flex justify-end w-full pt-4 px-20">
              <Button title="Close" className="outlined-btn" onClick={toggle} />
            </div>
          </Form>
        </Create>
      </CreateModal>
    );
  }, [CreateModal, toggle]);

  return [CreateModalComponent, { toggle, ...restProps }];
};

export default useCreateModal;
