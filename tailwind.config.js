/** @type {import('tailwindcss').Config} */

export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#0F52BA',
        secondary: '#FF6F61',
        highlight: '#0F52BA77',
      },
    },
  },
  plugins: [],
};
